customElements.define("mas-tter", class extends HTMLElement {
	connectedCallback() {
		this.innerHTML = `
			<div id="root">
				<div id="left">
					<prof-msd></prof-msd>
				</div>
				<div id="right">
					<write-msd></write-msd>
					<list-msd></list-msd>
				</div>
			</div>
		`
		this.querySelector("list-msd").addEventListener("reply", eve => {
			this.querySelector("write-msd").setReply("anond:" + eve.detail)
		})
		this.querySelector("write-msd").addEventListener("type", eve => {
			this.querySelector("prof-msd").spin()
		})
		
		const url = new URL(location).searchParams.get("url")
		if (url) {
			let title = url
			const matched = url.match(/^https\:\/\/anond\.hatelabo\.jp\/(\d+)$/)
			if (matched) {
				title = "anond:" + matched[1]
			}
			history.replaceState(null, "", location.pathname)
			Promise.resolve().then(() => {
				this.querySelector("write-msd").setReply(title)
			})
		}
	}
})

customElements.define("prof-msd", class extends HTMLElement {
	connectedCallback() {
		this.innerHTML = `
			<div class="prof">増</div>
			<div class="status">
				<div>@masuda</div>
				<div><a href="https://anond.hatelabo.jp/">https://anond.hatelabo.jp/</a></div>
			</div>
		`
		this.querySelector(".prof").addEventListener("click", () => {
			document.documentElement.scrollTo({ top:0, behavior: "smooth" })
		})
		this.deg = 0
	}
	
	spin() {
		this.deg += 360
		this.querySelector(".prof").style.transform = `rotate(${this.deg}deg)`
	}
})

customElements.define("write-msd", class extends HTMLElement {
	connectedCallback() {
		this.innerHTML = `
			<div class="write-msd">
				<textarea class="ta"></textarea>
				<div class="btm">
					<div>
						<label>
							<input type="checkbox" class="markdown" name="markdown">
							Use Markdown
						</label>
					</div>
					<div class="right">
						<span class="num">0 / 300</span>
						<button name="send">投稿する</button>
					</div>
				</div>
			</div>
		`
		this.addEventListener("input", () => {
			this.countChars()
			this.dispatchEvent(new Event("type"))
		})
		this.addEventListener("click", eve => {
			const text = this.querySelector(".ta").value
			if (text.trim() && eve.target.name === "send") {
				if (confirm("投稿しますか？")) {
					const [title, body] = this.detectTitle(text)
					const mode = this.querySelector(".markdown").checked ? "markdown" : "plain"
					new Scraper().send(title, this.formatBody(body, mode)).then(
						() => {
							alert("投稿しました")
						},
						(err) => {
							if(err.ok === false) {
								alert(`投稿に失敗しました (status: ${err.status})`)
							} else {
								alert("投稿に失敗しました")
							}
							console.error(err)
						}
					)
				}
			}
		})
		this.md = markdownit({html: true})
	}
	
	countChars() {
		const num = this.querySelector(".ta").value.length
		const elem = this.querySelector(".num")
		elem.textContent = `${num} / 300`
		elem.classList.toggle("over", num > 300)
	}
	
	detectTitle(text) {
		const [first, second] = text.split("\n", 2)
		if(second === "") {
			return [first, text.slice(first.length + 2)]
		} else {
			return ["", text]
		}
	}

	formatBody(text, mode) {
		if (mode === "markdown") {
			let html = this.md.render(text)
			const doc = new DOMParser().parseFromString(html, "text/html")
			for(const elem of doc.querySelectorAll("pre code")) {
				elem.innerHTML = elem.innerHTML.replace(/\n/g, "<br/>")
			}
			return doc.body.innerHTML.replace(/\n/g, "").replace(/<h(\d)>(.*?)<\/h\d>/g, (_, h, body) => {
				return `\n${"*".repeat(+h)} ${body}\n`
			})
		} else {
			return text.replace(/\n{2,}/g, x => Array(x.length).fill("\n").join("<br />"))
		}
	}

	setReply(title) {
		const ta = this.querySelector(".ta")
		if (!ta.value.length || confirm("入力中のテキストをクリアしても良いですか？")) {
			ta.value = title + "\n\n<<ここに本文>>"
			ta.focus()
			ta.select()
			ta.selectionStart = title.length + 2
			this.countChars()
		}
	}
})

customElements.define("list-msd", class extends HTMLElement {
	only_mine = true
	next_page = 1

	connectedCallback() {
		this.innerHTML = `
			<div class="list-msd">
				<div class="opt">
					<label><input type="radio" name="type" value="0"> 全増田</label>
					<label><input type="radio" name="type" value="1" checked> マイ増田</label>
					<button class="reload">↻</button>
				</div>
				<div class="list"></div>
				<div class="next">続きを取得</div>
			</div>
		`
		this.querySelector(".opt").addEventListener("change", () => {
			this.only_mine = !!+this.querySelector("[name=type]:checked").value
			this.reset()
		})
		this.querySelector(".reload").addEventListener("click", () => this.reset())
		this.querySelector(".next").addEventListener("click", () => {
			this.getMore()
		})
		this.querySelector(".list").addEventListener("click", eve => {
			if (eve.target.name === "reply") {
				this.dispatchEvent(new CustomEvent("reply", { detail: eve.target.dataset.id }))
			}
		})
		Promise.resolve().then(() => this.reset())
	}

	async getMore() {
		let list
		const next_page = this.next_page++
		if(this.only_mine) {
			list = await new Scraper().getMyList(next_page)
		} else {
			list = await new Scraper().getGlobalList(next_page)
		}
		
		const div = this.querySelector(".list")
		if(div.querySelector(".loading")) {
			div.innerHTML = ""
			this.querySelector(".next").hidden = false
		}
		
		for(const item of list) {
			if(this.querySelector(`article[id="${item.id}"]`)) continue

			const sub = document.createElement("article")
			sub.dataset.id = item.id
			sub.innerHTML = `
				<h1 class="title">
					<a target="_blank"></a>
					<button class="reply" name="reply">リプライ</button>
				</h1>
				<div class="body"></div>
			`
			Object.assign(
				sub.querySelector(".title a"),
				{
					textContent: "■ " + item.title,
					href: "https://anond.hatelabo.jp/" + item.id,
				}
			)
			sub.querySelector(".reply").dataset.id = item.id
			sub.querySelector(".body").innerHTML = item.body.replace(/href="\//g, `target="_blank" href="https://anond.hatelabo.jp/`)
			div.append(sub)
		}
	}
	
	reset() {
		this.next_page = 1
		this.querySelector(".list").innerHTML = `<div class="loading">↻</div>`
		this.querySelector(".next").hidden = true
		this.getMore()
	}
})

class Scraper {
	async getDoc(url) {
		return fetch(url)
			.then(e => e.text())
			.then(html => new DOMParser().parseFromString(html, "text/html"))
	}

	async getUserBase() {
		const top = "https://anond.hatelabo.jp/"
		const doc = await this.getDoc(top)
		const elem = doc.querySelector(".username a")
		if(!elem) throw { error: "ログインしていません" }
		return new URL(elem.getAttribute("href"), top)
	}

	async send(title, body) {
		const baseurl = await this.getUserBase()
		const editurl = baseurl.href + "edit"
		const doc = await this.getDoc(editurl)
		const rkm = doc.querySelector("#body form").elements.rkm.value
		const data = {
			rkm,
			mode: "confirm",
			id: "",
			title: title,
			body: body,
			edit: "この内容を登録する",
		}
		const res = await fetch(editurl, {
			method: "POST",
			body: new URLSearchParams(data).toString(),
			headers: {
				"content-type": "application/x-www-form-urlencoded"
			}
		})
		console.log(res)
		if(!res.ok) {
			throw res
		}
	}

	parseItems(doc) {
		const sections = doc.querySelectorAll(".day .body .section")
		return [...sections].map(sec => {
			const id = sec.querySelector("a").getAttribute("href").slice(1)
			const title = sec.querySelector(":scope>h3").textContent.slice(1).trim()
			const removes = sec.querySelectorAll(":scope>h3,:scope>.afc,:scope>.share-button,:scope>.share-button~*")
			for(const rem of removes) rem.remove()
			const body = sec.innerHTML
			return { id, title, body }
		})
	}

	async getMyList(page) {
		const baseurl = await this.getUserBase()
		baseurl.searchParams.set("page", page)
		const doc = await this.getDoc(baseurl)
		return this.parseItems(doc)
	}
	
	async getGlobalList(page) {
		const baseurl = new URL("https://anond.hatelabo.jp/")
		baseurl.searchParams.set("page", page)
		const doc = await this.getDoc(baseurl)
		return this.parseItems(doc)
	}
}
